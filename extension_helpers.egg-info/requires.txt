
[docs]
sphinx-astropy

[test]
pytest-astropy
pytest-cov
coverage<5
